package endercrypt.stardeweggoptimizer.player;


import endercrypt.library.commons.position.Coordinate;


public class PlayerConfiguration
{
	private final int radius;
	private final Coordinate position;
	
	private PlayerConfiguration(Builder builder)
	{
		this.radius = builder.radius;
		this.position = builder.position;
	}
	
	public Coordinate getPosition()
	{
		return position;
	}
	
	public int getRadius()
	{
		return radius;
	}
	
	public int getSize()
	{
		return radius * 2;
	}
	
	public static class Builder
	{
		private int radius;
		
		public Builder setRadius(int radius)
		{
			this.radius = radius;
			return this;
		}
		
		private Coordinate position;
		
		public Builder setPosition(int x, int y)
		{
			position = new Coordinate(x, y);
			return this;
		}
		
		public PlayerConfiguration build()
		{
			return new PlayerConfiguration(this);
		}
	}
}
