package endercrypt.stardeweggoptimizer.map;


import endercrypt.library.commons.data.DataSource;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class StardewMap
{
	private static final Logger logger = LogManager.getLogger(StardewMap.class);
	
	public static StardewMap read(String file) throws IOException
	{
		Path path = Paths.get(System.getProperty("user.dir")).resolve(file);
		String filename = path.getFileName().toString();
		logger.info("Loading in map from " + filename + " ...");
		BufferedImage image = DataSource.DISK.read(path).asImage();
		StardewMap map = new StardewMap(image.getWidth(), image.getHeight());
		
		logger.debug("Image: " + image.getWidth() + ", " + image.getHeight());
		for (int x = 0; x < map.getWidth(); x++)
		{
			for (int y = 0; y < map.getHeight(); y++)
			{
				try
				{
					map.pixels[x][y] = MapItem.parse(image.getRGB(x, y));
				}
				catch (IllegalArgumentException e)
				{
					throw new IllegalArgumentException("illegal pixel at: " + x + ", " + y, e);
				}
			}
		}
		return map;
	}
	
	private final int width;
	private final int height;
	private final MapItem[][] pixels;
	
	private StardewMap(int width, int height)
	{
		this.width = width;
		this.height = height;
		this.pixels = new MapItem[width][height];
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public MapItem get(int x, int y)
	{
		if (x < 0 || y < 0 || x >= getWidth() || y >= getHeight())
		{
			return MapItem.OBSTACLE;
		}
		return pixels[x][y];
	}
	
	public boolean isPassable(int x, int y)
	{
		return get(x, y) != MapItem.OBSTACLE; // egg doesent count as you can pick it up and walk through where it was
	}
	
	public boolean isObstacle(int x, int y)
	{
		return isPassable(x, y) == false;
	}
	
	public boolean isEgg(int x, int y)
	{
		return get(x, y) == MapItem.EGG;
	}
	
}
