package endercrypt.stardeweggoptimizer.map;


import java.awt.Color;


public enum MapItem
{
	WALKABLE(Color.WHITE),
	OBSTACLE(Color.BLACK),
	EGG(Color.RED);
	
	private static final MapItem[] values = values();
	
	private final int rgb;
	
	private MapItem(Color color)
	{
		this(color.getRGB());
	}
	
	private MapItem(int rgb)
	{
		this.rgb = rgb;
	}
	
	public int getRgb()
	{
		return rgb;
	}
	
	public static MapItem parse(int rgb)
	{
		for (MapItem item : values)
		{
			if (item.getRgb() == rgb)
			{
				return item;
			}
		}
		throw new IllegalArgumentException("unknown RGB: " + rgb);
	}
}
