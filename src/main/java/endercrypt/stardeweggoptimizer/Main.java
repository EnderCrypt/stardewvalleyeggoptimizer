package endercrypt.stardeweggoptimizer;


import endercrypt.stardeweggoptimizer.egg.EggBasket;
import endercrypt.stardeweggoptimizer.map.StardewMap;
import endercrypt.stardeweggoptimizer.path.ObstacleMap;
import endercrypt.stardeweggoptimizer.pather.PathMaster;
import endercrypt.stardeweggoptimizer.player.PlayerConfiguration;
import endercrypt.stardeweggoptimizer.solution.Solution;
import endercrypt.stardeweggoptimizer.solution.Solver;
import endercrypt.stardeweggoptimizer.solution.solver.SolverRandomWeighted;
import endercrypt.stardeweggoptimizer.utility.Exporter;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Main
{
	private static final Logger logger = LogManager.getLogger(Main.class);
	
	public static void main(String[] args) throws IOException, InterruptedException
	{
		StardewMap map = StardewMap.read("simple.png");
		logger.info("Created map: " + map);
		
		EggBasket eggs = EggBasket.generate(map);
		logger.info("Found " + eggs.size() + " eggs");
		
		PlayerConfiguration player = new PlayerConfiguration.Builder()
			.setRadius(5)
			.setPosition(390, 330)
			.build();
		logger.info("Created player configuration: " + player);
		
		// obstacle map
		ObstacleMap obstacleMap = ObstacleMap.generate(map, player);
		logger.info("Created obstacle map: " + obstacleMap);
		
		// paths
		PathMaster pathMaster = new PathMaster(obstacleMap);
		logger.info("Created PathMaster");
		
		// resolve best paths
		Solver solver = new SolverRandomWeighted(100_000_000, 5, 2.5);
		logger.info("Starting to calculate solution using " + solver.getClass().getSimpleName());
		Solution solution = solver.calculate(player, pathMaster, eggs);
		logger.info("found solution with length: " + Math.round(solution.getLength()));
		
		// done
		Exporter.export(solution.render(map), "./solution_" + Math.round(solution.getLength()) + ".png");
		logger.info("good luck with the run!");
	}
}
