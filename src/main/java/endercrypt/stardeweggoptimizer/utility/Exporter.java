package endercrypt.stardeweggoptimizer.utility;


import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;


public class Exporter
{
	public static void export(BufferedImage image, String file) throws IOException
	{
		// path
		Path path = Paths.get(System.getProperty("user.dir")).resolve(file);
		String filename = path.getFileName().toString();
		String extension = FilenameUtils.getExtension(filename);
		
		// directory
		Files.createDirectories(path.getParent());
		
		// export
		ImageIO.write(image, extension, path.toFile());
		image.flush();
	}
}
