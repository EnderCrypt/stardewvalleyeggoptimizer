package endercrypt.stardeweggoptimizer.solution;


import endercrypt.library.commons.position.Coordinate;
import endercrypt.stardeweggoptimizer.map.StardewMap;
import endercrypt.stardeweggoptimizer.path.MapPath;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.List;


public class Solution
{
	private final List<MapPath> paths;
	private final double length;
	
	public Solution(List<MapPath> paths)
	{
		this.paths = List.copyOf(paths);
		this.length = paths.stream().mapToDouble(MapPath::getLength).sum();
	}
	
	public List<MapPath> getPaths()
	{
		return paths;
	}
	
	public double getLength()
	{
		return length;
	}
	
	public BufferedImage render(StardewMap map)
	{
		int width = map.getWidth();
		int height = map.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		// map
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				image.setRGB(x, y, map.get(x, y).getRgb());
			}
		}
		
		// paths
		int pathColor = Color.GREEN.getRGB();
		for (MapPath path : paths)
		{
			for (Coordinate coordinate : path.getCoordinates())
			{
				image.setRGB(coordinate.getX(), coordinate.getY(), pathColor);
			}
		}
		
		return image;
	}
}
