package endercrypt.stardeweggoptimizer.solution;


import endercrypt.stardeweggoptimizer.egg.EggBasket;
import endercrypt.stardeweggoptimizer.pather.PathMaster;
import endercrypt.stardeweggoptimizer.player.PlayerConfiguration;


public interface Solver
{
	public static final int REQUIRED_EGGS = 19;
	public static final int REQUIRED_PATHS = REQUIRED_EGGS - 1;
	
	public Solution calculate(PlayerConfiguration player, PathMaster pathMaster, EggBasket eggs) throws InterruptedException;
}
