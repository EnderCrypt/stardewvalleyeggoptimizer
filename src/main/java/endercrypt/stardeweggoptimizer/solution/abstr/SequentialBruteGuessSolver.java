package endercrypt.stardeweggoptimizer.solution.abstr;


import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionRequest;


public abstract class SequentialBruteGuessSolver extends GenericBruteGuessSolver
{
	public SequentialBruteGuessSolver(int totalTries)
	{
		super(totalTries);
	}
	
	@Override
	protected final void process(SolutionRequest request)
	{
		calculate(request);
	}
	
	protected abstract void calculate(SolutionRequest request);
}
