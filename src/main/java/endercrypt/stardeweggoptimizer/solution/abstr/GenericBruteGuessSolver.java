package endercrypt.stardeweggoptimizer.solution.abstr;


import endercrypt.stardeweggoptimizer.solution.Solution;
import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionMaster;
import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionRequest;


public abstract class GenericBruteGuessSolver extends AbstractSolver
{
	private final int totalTries;
	
	public GenericBruteGuessSolver(int totalTries)
	{
		this.totalTries = totalTries;
		if (totalTries <= 0)
		{
			throw new IllegalArgumentException("totalTries cannot be " + totalTries);
		}
	}
	
	public final int getTotalTries()
	{
		return totalTries;
	}
	
	@Override
	public final Solution calculate(SolutionMaster master) throws InterruptedException
	{
		for (int i = 0; i < totalTries; i++)
		{
			process(master.request(i + " / " + totalTries));
		}
		return master.requestBestSolution();
	}
	
	protected abstract void process(SolutionRequest request);
}
