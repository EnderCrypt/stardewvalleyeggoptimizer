package endercrypt.stardeweggoptimizer.solution.abstr;


import endercrypt.stardeweggoptimizer.egg.EggBasket;
import endercrypt.stardeweggoptimizer.pather.PathMaster;
import endercrypt.stardeweggoptimizer.player.PlayerConfiguration;
import endercrypt.stardeweggoptimizer.solution.Solution;
import endercrypt.stardeweggoptimizer.solution.Solver;
import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionMaster;


public abstract class AbstractSolver implements Solver
{
	@Override
	public final Solution calculate(PlayerConfiguration player, PathMaster pathMaster, EggBasket eggs) throws InterruptedException
	{
		SolutionMaster master = new SolutionMaster(player, pathMaster, eggs);
		calculate(master);
		Solution solution = master.requestBestSolution();
		finish();
		return solution;
	}
	
	protected abstract Solution calculate(SolutionMaster request) throws InterruptedException;
	
	protected void finish()
	{
		// do nothing
	}
}
