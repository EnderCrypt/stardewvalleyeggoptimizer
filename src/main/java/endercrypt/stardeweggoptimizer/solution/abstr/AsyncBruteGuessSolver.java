package endercrypt.stardeweggoptimizer.solution.abstr;


import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionRequest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public abstract class AsyncBruteGuessSolver extends GenericBruteGuessSolver
{
	private final int threads;
	private final ExecutorService executor;
	
	public AsyncBruteGuessSolver(int totalTries, int threads)
	{
		super(totalTries);
		this.threads = threads;
		executor = Executors.newFixedThreadPool(threads);
	}
	
	public int getThreads()
	{
		return threads;
	}
	
	@Override
	protected final void process(SolutionRequest request)
	{
		executor.submit(() -> calculate(request));
	}
	
	@Override
	protected void finish()
	{
		super.finish();
		executor.shutdown();
	}
	
	protected abstract void calculate(SolutionRequest request);
}
