package endercrypt.stardeweggoptimizer.solution.abstr.request;


import endercrypt.stardeweggoptimizer.egg.EggBasket;
import endercrypt.stardeweggoptimizer.pather.PathMaster;
import endercrypt.stardeweggoptimizer.player.PlayerConfiguration;
import endercrypt.stardeweggoptimizer.solution.Solution;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class SolutionMaster
{
	private static final Logger logger = LogManager.getLogger(SolutionMaster.class);
	
	private final PlayerConfiguration playerConfiguration;
	private final PathMaster pathMaster;
	private final EggBasket eggBasket;
	
	private Set<SolutionRequest> activeRequests = new HashSet<>();
	private Solution bestSolution = null;
	
	public SolutionMaster(PlayerConfiguration playerConfiguration, PathMaster pathMaster, EggBasket eggBasket)
	{
		this.playerConfiguration = playerConfiguration;
		this.pathMaster = pathMaster;
		this.eggBasket = eggBasket;
	}
	
	public final PlayerConfiguration getPlayerConfiguration()
	{
		return playerConfiguration;
	}
	
	public final PathMaster getPathMaster()
	{
		return pathMaster;
	}
	
	public final EggBasket getEggBasket()
	{
		return eggBasket;
	}
	
	public Solution getBestSolution()
	{
		return bestSolution;
	}
	
	protected synchronized boolean submitSolution(SolutionWorker worker)
	{
		synchronized (activeRequests)
		{
			activeRequests.remove(worker.getRequest());
			activeRequests.notify();
		}
		
		Solution solution = worker.generateSolution();
		if (bestSolution == null || solution.getLength() < bestSolution.getLength())
		{
			bestSolution = solution;
			long time = (System.currentTimeMillis() - worker.getStartTime());
			logger.info(worker.getRequest().getName() + " length: " + Math.round(solution.getLength()) + " time: " + time + " ms");
			return true;
		}
		return false;
	}
	
	public Solution requestBestSolution() throws InterruptedException
	{
		synchronized (activeRequests)
		{
			while (activeRequests.isEmpty() == false)
			{
				activeRequests.wait();
			}
			activeRequests.notify();
			
			return bestSolution;
		}
	}
	
	public SolutionRequest request(String name)
	{
		SolutionRequest requests = new SolutionRequest(this, name);
		activeRequests.add(requests);
		return requests;
	}
}
