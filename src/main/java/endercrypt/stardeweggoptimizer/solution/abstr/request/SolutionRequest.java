package endercrypt.stardeweggoptimizer.solution.abstr.request;

public class SolutionRequest
{
	private final SolutionMaster master;
	private final String name;
	
	private SolutionWorker worker;
	
	protected SolutionRequest(SolutionMaster master, String name)
	{
		this.master = master;
		this.name = name;
	}
	
	public SolutionMaster getMaster()
	{
		return master;
	}
	
	public String getName()
	{
		return name;
	}
	
	public synchronized SolutionWorker start()
	{
		if (worker != null)
		{
			throw new IllegalStateException("solution worker for " + this + " is already active");
		}
		worker = new SolutionWorker(this);
		return worker;
	}
}
