package endercrypt.stardeweggoptimizer.solution.abstr.request;


import endercrypt.library.commons.position.Coordinate;
import endercrypt.stardeweggoptimizer.egg.Egg;
import endercrypt.stardeweggoptimizer.path.MapPath;
import endercrypt.stardeweggoptimizer.path.find.PathingConfiguration;
import endercrypt.stardeweggoptimizer.solution.Solution;
import endercrypt.stardeweggoptimizer.solution.Solver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class SolutionWorker
{
	private final SolutionRequest request;
	
	private final long startTime = System.currentTimeMillis();
	private final List<MapPath> paths = new ArrayList<>();
	private final Set<Egg> eggs;
	private Coordinate coordinate;
	
	protected SolutionWorker(SolutionRequest request)
	{
		this.request = request;
		this.eggs = new HashSet<>(getMaster().getEggBasket());
		this.coordinate = getMaster().getPlayerConfiguration().getPosition();
	}
	
	public SolutionMaster getMaster()
	{
		return getRequest().getMaster();
	}
	
	public SolutionRequest getRequest()
	{
		return request;
	}
	
	public MapPath calculatePath(Egg egg)
	{
		return getMaster().getPathMaster().getPath(getCoordinate(), egg.getCenterCoordinate());
	}
	
	public long getStartTime()
	{
		return startTime;
	}
	
	public List<MapPath> getPaths()
	{
		return Collections.unmodifiableList(paths);
	}
	
	public List<Egg> getEggs()
	{
		return new ArrayList<>(eggs);
	}
	
	public Coordinate getCoordinate()
	{
		return coordinate;
	}
	
	public synchronized boolean isComplete()
	{
		int eggCount = paths.size() + 1;
		return eggCount >= Solver.REQUIRED_EGGS;
	}
	
	public synchronized boolean pushPath(Egg egg)
	{
		return pushPath(egg, calculatePath(egg));
	}
	
	public synchronized boolean pushPath(Egg egg, MapPath path)
	{
		PathingConfiguration configuration = path.getConfiguration();
		if (getCoordinate().equals(configuration.getStart()) == false)
		{
			throw new IllegalArgumentException(path + " does not start from " + getCoordinate());
		}
		if (egg.isAt(configuration.getEnd().getX(), configuration.getEnd().getY()) == false)
		{
			throw new IllegalArgumentException(path + " does not end at egg " + egg);
		}
		if (eggs.contains(egg) == false)
		{
			throw new IllegalArgumentException("egg basket does not contain " + egg);
		}
		if (isComplete())
		{
			throw new IllegalArgumentException("path already complete");
		}
		
		coordinate = configuration.getEnd();
		paths.add(path);
		eggs.remove(egg);
		
		if (isComplete())
		{
			getMaster().submitSolution(this);
			return true;
		}
		return false;
	}
	
	public Solution generateSolution()
	{
		if (isComplete() == false)
		{
			throw new IllegalArgumentException("path not complete");
		}
		return new Solution(paths);
	}
}
