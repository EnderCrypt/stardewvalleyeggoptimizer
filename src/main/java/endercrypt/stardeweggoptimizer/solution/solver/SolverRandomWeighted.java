package endercrypt.stardeweggoptimizer.solution.solver;


import endercrypt.library.commons.randomizer.Randomizer;
import endercrypt.stardeweggoptimizer.egg.Egg;
import endercrypt.stardeweggoptimizer.path.MapPath;
import endercrypt.stardeweggoptimizer.solution.abstr.AsyncBruteGuessSolver;
import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionRequest;
import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionWorker;


public class SolverRandomWeighted extends AsyncBruteGuessSolver
{
	private final double weight;
	
	public SolverRandomWeighted(int totalTries, int threads, double weight)
	{
		super(totalTries, threads);
		this.weight = weight;
	}
	
	@Override
	protected void calculate(SolutionRequest request)
	{
		SolutionWorker worker = request.start();
		while (worker.isComplete() == false)
		{
			worker.pushPath(getNextEgg(worker));
		}
	}
	
	protected Egg getNextEgg(SolutionWorker worker)
	{
		Randomizer<Egg> randomizer = new Randomizer<>();
		for (Egg egg : worker.getEggs())
		{
			MapPath path = worker.calculatePath(egg);
			double distance = path.getLength();
			double eggWeight = Math.pow(1.0 / distance, weight);
			randomizer.enter(eggWeight, egg);
		}
		return randomizer.pull();
	}
}
