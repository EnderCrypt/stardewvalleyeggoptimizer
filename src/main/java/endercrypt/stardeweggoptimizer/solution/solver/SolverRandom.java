package endercrypt.stardeweggoptimizer.solution.solver;


import endercrypt.library.commons.RandomEntry;
import endercrypt.stardeweggoptimizer.solution.abstr.AsyncBruteGuessSolver;
import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionRequest;
import endercrypt.stardeweggoptimizer.solution.abstr.request.SolutionWorker;


public class SolverRandom extends AsyncBruteGuessSolver
{
	public SolverRandom(int totalTries, int threads)
	{
		super(totalTries, threads);
	}
	
	@Override
	protected void calculate(SolutionRequest request)
	{
		SolutionWorker worker = request.start();
		while (worker.isComplete() == false)
		{
			worker.pushPath(RandomEntry.from(worker.getEggs()));
		}
	}
}
