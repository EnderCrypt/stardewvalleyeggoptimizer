package endercrypt.stardeweggoptimizer.pather;


import java.util.Objects;

import endercrypt.library.commons.position.Coordinate;
import endercrypt.stardeweggoptimizer.path.find.MapGoal;


class PathMasterMapGoal implements MapGoal
{
	private final Coordinate start;
	private final Coordinate end;
	
	public PathMasterMapGoal(MapGoal goal)
	{
		this(goal.getStart(), goal.getEnd());
	}
	
	public PathMasterMapGoal(Coordinate start, Coordinate end)
	{
		this.start = Objects.requireNonNull(start, "start");
		this.end = Objects.requireNonNull(end, "end");
	}
	
	@Override
	public Coordinate getStart()
	{
		return start;
	}
	
	@Override
	public Coordinate getEnd()
	{
		return end;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((end == null) ? 0 : end.hashCode());
		result = prime * result + ((start == null) ? 0 : start.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof PathMasterMapGoal)) return false;
		PathMasterMapGoal other = (PathMasterMapGoal) obj;
		if (end == null)
		{
			if (other.end != null) return false;
		}
		else if (!end.equals(other.end)) return false;
		if (start == null)
		{
			if (other.start != null) return false;
		}
		else if (!start.equals(other.start)) return false;
		return true;
	}
}
