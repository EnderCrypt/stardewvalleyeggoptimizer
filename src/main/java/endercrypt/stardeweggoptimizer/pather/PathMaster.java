package endercrypt.stardeweggoptimizer.pather;


import endercrypt.library.commons.position.Coordinate;
import endercrypt.stardeweggoptimizer.path.MapPath;
import endercrypt.stardeweggoptimizer.path.ObstacleMap;
import endercrypt.stardeweggoptimizer.path.find.MapGoal;
import endercrypt.stardeweggoptimizer.path.find.PathingConfiguration;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class PathMaster
{
	private final ObstacleMap obstacleMap;
	private final Map<PathMasterMapGoal, MapPath> paths = new HashMap<>();
	
	public PathMaster(ObstacleMap obstacleMap)
	{
		this.obstacleMap = Objects.requireNonNull(obstacleMap, "obstacleMap");
	}
	
	public MapPath getPath(Coordinate start, Coordinate end)
	{
		PathMasterMapGoal goal = new PathMasterMapGoal(start, end);
		MapPath path = paths.get(goal);
		if (path == null)
		{
			path = resolvePath(goal);
		}
		return path;
	}
	
	private synchronized MapPath resolvePath(PathMasterMapGoal goal)
	{
		MapPath path = paths.get(goal);
		if (path != null)
		{
			return path;
		}
		path = createPath(goal);
		setupPath(path);
		setupPath(MapPath.reverse(path));
		return path;
	}
	
	private void setupPath(MapPath path)
	{
		PathMasterMapGoal goal = new PathMasterMapGoal(path.getConfiguration());
		paths.put(goal, path);
	}
	
	private MapPath createPath(MapGoal goal)
	{
		return new PathingConfiguration.Builder()
			.setMap(obstacleMap)
			.setGoal(goal)
			.path();
	}
}
