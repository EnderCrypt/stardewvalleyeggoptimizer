package endercrypt.stardeweggoptimizer.path;


import endercrypt.library.commons.graphics.SmartImageGraphics;
import endercrypt.library.commons.position.Circle;
import endercrypt.stardeweggoptimizer.map.StardewMap;
import endercrypt.stardeweggoptimizer.player.PlayerConfiguration;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ObstacleMap
{
	private static final Logger logger = LogManager.getLogger(ObstacleMap.class);
	
	public static ObstacleMap generate(StardewMap map, PlayerConfiguration player)
	{
		logger.info("Calculating obstacle map for " + player + " on " + map);
		
		ObstacleMap obstacle = new ObstacleMap(map.getWidth(), map.getHeight());
		for (int x = 0; x < map.getWidth(); x++)
		{
			for (int y = 0; y < map.getHeight(); y++)
			{
				obstacle.passable[x][y] = isPassable(map, player, x, y);
			}
		}
		return obstacle;
	}
	
	private static boolean isPassable(StardewMap map, PlayerConfiguration player, int x, int y)
	{
		final double increments = Math.toRadians(10);
		for (int i = 0; i < player.getRadius(); i++)
		{
			double length = Math.PI * i;
			double segments = length / increments;
			double skip = Circle.FULL / segments;
			for (double angle = 0; angle < Circle.FULL; angle += skip)
			{
				int ox = (int) (x + Math.cos(angle) * i);
				int oy = (int) (y + Math.sin(angle) * i);
				if (map.isObstacle(ox, oy))
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	private final int width;
	private final int height;
	private final boolean[][] passable;
	
	private ObstacleMap(int width, int height)
	{
		this.width = width;
		this.height = height;
		this.passable = new boolean[width][height];
	}
	
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public boolean isPassable(int x, int y)
	{
		if (x < 0 || y < 0 || x >= width || y >= height)
		{
			return false;
		}
		
		return passable[x][y];
	}
	
	public BufferedImage render()
	{
		SmartImageGraphics graphics = new SmartImageGraphics(new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB));
		for (int x = 0; x < getWidth(); x++)
		{
			for (int y = 0; y < getHeight(); y++)
			{
				graphics.setColor(isPassable(x, y) ? Color.WHITE : Color.BLACK);
				graphics.drawPixel(x, y);
			}
		}
		graphics.dispose();
		return graphics.getImage();
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + height;
		result = prime * result + Arrays.deepHashCode(passable);
		result = prime * result + width;
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj) return true;
		if (!(obj instanceof ObstacleMap)) return false;
		ObstacleMap other = (ObstacleMap) obj;
		if (height != other.height) return false;
		if (!Arrays.deepEquals(passable, other.passable)) return false;
		if (width != other.width) return false;
		return true;
	}
}
