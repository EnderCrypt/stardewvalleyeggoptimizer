package endercrypt.stardeweggoptimizer.path;


import endercrypt.library.commons.position.Coordinate;
import endercrypt.stardeweggoptimizer.path.find.MapNode;
import endercrypt.stardeweggoptimizer.path.find.PathNotFoundException;
import endercrypt.stardeweggoptimizer.path.find.PathOrganizer;
import endercrypt.stardeweggoptimizer.path.find.PathingConfiguration;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;


public class MapPath
{
	public static MapPath fetch(PathingConfiguration configuration) throws PathNotFoundException
	{
		Path cachePath = configuration.resolveCachePath();
		MapPath path = null;
		if (Files.exists(cachePath))
		{
			path = read(configuration, cachePath);
		}
		
		if (path == null)
		{
			path = find(configuration);
			path.save(cachePath);
		}
		
		return Objects.requireNonNull(path, "path");
	}
	
	private static MapPath read(PathingConfiguration configuration, Path path)
	{
		try (DataInputStream input = new DataInputStream(new BufferedInputStream(new FileInputStream(path.toFile()))))
		{
			int size = input.readInt();
			List<Coordinate> coordinates = new ArrayList<>();
			for (int i = 0; i < size; i++)
			{
				int x = input.readInt();
				int y = input.readInt();
				coordinates.add(new Coordinate(x, y));
			}
			return new MapPath(configuration, coordinates);
		}
		catch (IOException e)
		{
			throw new PathCacheException("failed to read path cache: " + path, e);
		}
	}
	
	private static MapPath find(PathingConfiguration configuration) throws PathNotFoundException
	{
		PathOrganizer organizer = new PathOrganizer(configuration);
		while (organizer.hasNext())
		{
			MapNode node = organizer.next();
			Coordinate coordinate = node.getCoordinate();
			if (configuration.getMap().isPassable(coordinate.getX(), coordinate.getY()))
			{
				organizer.addNode(node);
			}
		}
		return new MapPath(configuration, organizer.generatePath());
	}
	
	public static MapPath reverse(MapPath oldPath)
	{
		PathingConfiguration oldConfiguration = oldPath.getConfiguration();
		
		PathingConfiguration configuration = new PathingConfiguration.Builder()
			.setMap(oldConfiguration.getMap())
			.setStart(oldConfiguration.getEnd())
			.setEnd(oldConfiguration.getStart())
			.build();
		
		List<Coordinate> coordinates = new ArrayList<>(oldPath.getCoordinates());
		Collections.reverse(coordinates);
		
		return new MapPath(configuration, coordinates);
	}
	
	private final PathingConfiguration configuration;
	private final List<Coordinate> coordinates;
	
	private MapPath(PathingConfiguration configuration, List<Coordinate> coordinates)
	{
		this.configuration = configuration;
		this.coordinates = new ArrayList<>(coordinates);
		if (coordinates.isEmpty())
		{
			throw new IllegalArgumentException("path cannot be empty");
		}
	}
	
	public PathingConfiguration getConfiguration()
	{
		return configuration;
	}
	
	public List<Coordinate> getCoordinates()
	{
		return coordinates;
	}
	
	public double getLength()
	{
		return getCoordinates().size(); // TODO: count diagonals properly
	}
	
	private void save(Path path)
	{
		try
		{
			Path temporaryFile = path.getParent().resolve(path.getFileName() + ".temp");
			if (Files.exists(temporaryFile))
			{
				throw new PathCacheException("temporary file " + temporaryFile + " exists, when it shouldnt");
			}
			try (DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(temporaryFile.toFile()))))
			{
				output.writeInt(coordinates.size());
				for (Coordinate coordinate : coordinates)
				{
					output.writeInt(coordinate.getX());
					output.writeInt(coordinate.getY());
				}
			}
			Files.move(temporaryFile, path, StandardCopyOption.ATOMIC_MOVE);
		}
		catch (IOException e)
		{
			throw new PathCacheException("failed to write path cache: " + path, e);
		}
	}
}
