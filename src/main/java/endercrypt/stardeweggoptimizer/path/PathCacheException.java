package endercrypt.stardeweggoptimizer.path;

public class PathCacheException extends RuntimeException
{
	public PathCacheException(String message)
	{
		super(message);
	}
	
	public PathCacheException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
