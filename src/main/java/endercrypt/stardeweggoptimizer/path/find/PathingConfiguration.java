package endercrypt.stardeweggoptimizer.path.find;


import endercrypt.library.commons.position.Coordinate;
import endercrypt.stardeweggoptimizer.path.MapPath;
import endercrypt.stardeweggoptimizer.path.ObstacleMap;
import endercrypt.stardeweggoptimizer.path.PathCacheException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;


public class PathingConfiguration implements MapGoal
{
	private final ObstacleMap map;
	private final Coordinate start;
	private final Coordinate end;
	
	public PathingConfiguration(Builder builder)
	{
		this.map = Objects.requireNonNull(builder.map, "map");
		this.start = Objects.requireNonNull(builder.start, "start");
		this.end = Objects.requireNonNull(builder.end, "end");
	}
	
	public ObstacleMap getMap()
	{
		return map;
	}
	
	@Override
	public Coordinate getStart()
	{
		return start;
	}
	
	@Override
	public Coordinate getEnd()
	{
		return end;
	}
	
	public Path resolveCachePath()
	{
		Path directory = Paths.get("./pathfinding-cache/");
		if (Files.exists(directory) == false)
		{
			try
			{
				Files.createDirectory(directory);
			}
			catch (IOException e)
			{
				throw new PathCacheException("failed to setup cache directory " + directory, e);
			}
		}
		String startValue = start.getX() + "," + start.getY();
		String endValue = end.getX() + "," + end.getY();
		return directory.resolve(getMap().hashCode() + "_" + startValue + "_" + endValue + ".path");
	}
	
	public static class Builder
	{
		private ObstacleMap map;
		
		public Builder setMap(ObstacleMap map)
		{
			this.map = map;
			return this;
		}
		
		private Coordinate start;
		
		public Builder setStart(Coordinate start)
		{
			this.start = start;
			return this;
		}
		
		private Coordinate end;
		
		public Builder setEnd(Coordinate end)
		{
			this.end = end;
			return this;
		}
		
		public Builder setGoal(MapGoal goal)
		{
			setStart(goal.getStart());
			setEnd(goal.getEnd());
			return this;
		}
		
		public PathingConfiguration build()
		{
			return new PathingConfiguration(this);
		}
		
		public MapPath path()
		{
			try
			{
				return MapPath.fetch(build());
			}
			catch (PathNotFoundException e)
			{
				throw new IllegalArgumentException("couldnt path from " + start + " to " + end, e);
			}
		}
	}
}
