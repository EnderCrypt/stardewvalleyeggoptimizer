package endercrypt.stardeweggoptimizer.path.find;


import endercrypt.library.commons.position.Coordinate;


public interface MapGoal
{
	public Coordinate getStart();
	
	public Coordinate getEnd();
}
