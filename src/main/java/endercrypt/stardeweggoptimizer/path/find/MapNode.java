package endercrypt.stardeweggoptimizer.path.find;


import java.util.Objects;

import endercrypt.library.commons.position.Coordinate;


public class MapNode
{
	private final MapGoal goal;
	private final Coordinate coordinate;
	private final MapNode previous;
	
	private final double travelDistance;
	private final double distanceToEnd;
	private final double cost;
	
	public MapNode(MapGoal goal, Coordinate coordinate)
	{
		this(goal, coordinate, null);
	}
	
	public MapNode(Coordinate coordinate, MapNode previous)
	{
		this(previous.getGoal(), coordinate, previous);
	}
	
	private MapNode(MapGoal goal, Coordinate coordinate, MapNode previous)
	{
		this.goal = Objects.requireNonNull(goal, "goal");
		this.coordinate = Objects.requireNonNull(coordinate, "coordinate");
		this.previous = previous;
		
		this.travelDistance = previous == null ? 0 : previous.calculateTravelDistance(getCoordinate());
		this.distanceToEnd = getGoal().getEnd().distanceTo(getCoordinate());
		this.cost = getTravelDistance() + getDistanceToEnd();
	}
	
	public MapGoal getGoal()
	{
		return goal;
	}
	
	public Coordinate getCoordinate()
	{
		return coordinate;
	}
	
	public MapNode getPrevious()
	{
		return previous;
	}
	
	private double calculateTravelDistance(Coordinate coordinate)
	{
		return getTravelDistance() + getCoordinate().distanceTo(coordinate);
	}
	
	public double getTravelDistance() // G cost
	{
		return travelDistance;
	}
	
	public double getDistanceToEnd() // H cost
	{
		return distanceToEnd;
	}
	
	public double getCost() // F cost (G + H)
	{
		return cost;
	}
	
	@Override
	public String toString()
	{
		return "MapNode [" + coordinate.getX() + ", " + coordinate.getY() + "]";
	}
}
