package endercrypt.stardeweggoptimizer.path.find;


import endercrypt.library.commons.iterator.AbstractIterator;
import endercrypt.library.commons.position.Coordinate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;


public class PathOrganizer extends AbstractIterator<MapNode>
{
	private final MapGoal goal;
	
	private final Set<MapNode> nodes = new HashSet<>();
	private final Set<Coordinate> occupied = new HashSet<>();
	private final Map<Coordinate, MapNode> available = new HashMap<>();
	
	private MapNode complete;
	
	public PathOrganizer(MapGoal goal)
	{
		this.goal = Objects.requireNonNull(goal, "goal");
		addNode(new MapNode(goal, goal.getStart()));
	}
	
	public MapGoal getGoal()
	{
		return goal;
	}
	
	public boolean isComplete()
	{
		return complete != null;
	}
	
	public List<Coordinate> generatePath() throws PathNotFoundException
	{
		if (isComplete() == false)
		{
			throw new PathNotFoundException();
		}
		List<Coordinate> result = new ArrayList<>();
		MapNode node = complete;
		while (node != null)
		{
			result.add(node.getCoordinate());
			node = node.getPrevious();
		}
		Collections.reverse(result);
		return result;
	}
	
	public void addNode(MapNode node)
	{
		if (isComplete() == false && node.getCoordinate().equals(getGoal().getEnd()))
		{
			complete = node;
		}
		
		nodes.add(node);
		occupied.add(node.getCoordinate());
		
		for (Coordinate neighbour : getNeighbours(node.getCoordinate()))
		{
			if (occupied.contains(neighbour))
			{
				continue;
			}
			MapNode neighbourNode = new MapNode(neighbour, node);
			
			MapNode old = available.get(neighbour);
			if (old == null || neighbourNode.getCost() < old.getCost())
			{
				available.put(neighbourNode.getCoordinate(), neighbourNode);
			}
		}
	}
	
	private static List<Coordinate> getNeighbours(Coordinate coordinate)
	{
		List<Coordinate> result = new ArrayList<>();
		for (int x = -1; x < 2; x++)
		{
			for (int y = -1; y < 2; y++)
			{
				if (x != 0 || y != 0)
				{
					result.add(coordinate.add(x, y));
				}
			}
		}
		return result;
	}
	
	@Override
	protected Optional<MapNode> advance()
	{
		if (isComplete())
		{
			return Optional.empty();
		}
		if (available.isEmpty())
		{
			return Optional.empty();
		}
		MapNode best = null;
		for (MapNode node : available.values())
		{
			if (best == null || isBetter(best, node))
			{
				best = node;
			}
		}
		Objects.requireNonNull(best, "best");
		Coordinate coordinate = best.getCoordinate();
		occupied.add(coordinate);
		available.remove(coordinate);
		return Optional.of(best);
	}
	
	private static boolean isBetter(MapNode old, MapNode node)
	{
		double cost = old.getCost() - node.getCost();
		if (cost != 0)
		{
			return cost > 0;
		}
		double dist = old.getDistanceToEnd() - node.getDistanceToEnd();
		return dist > 0;
	}
}
