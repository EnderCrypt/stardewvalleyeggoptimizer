package endercrypt.stardeweggoptimizer.egg;


import java.util.HashSet;

import endercrypt.stardeweggoptimizer.map.StardewMap;


@SuppressWarnings("serial")
public class EggBasket extends HashSet<Egg>
{
	public static EggBasket generate(StardewMap map)
	{
		EggBasket eggBasket = new EggBasket();
		for (int x = 0; x < map.getWidth(); x++)
		{
			for (int y = 0; y < map.getHeight(); y++)
			{
				if (map.isEgg(x, y) && eggBasket.isPresentAt(x, y) == false)
				{
					Egg egg = Egg.sample(map, x, y);
					eggBasket.add(egg);
				}
			}
		}
		return eggBasket;
	}
	
	@Override
	public boolean add(Egg egg)
	{
		if (isPresentAt(egg.getX(), egg.getY()))
		{
			throw new IllegalArgumentException("egg already present at location");
		}
		return super.add(egg);
	}
	
	public boolean isPresentAt(int x, int y)
	{
		for (Egg egg : this)
		{
			if (egg.isAt(x, y))
			{
				return true;
			}
		}
		return false;
	}
}
