package endercrypt.stardeweggoptimizer.egg;


import endercrypt.library.commons.position.Coordinate;
import endercrypt.stardeweggoptimizer.map.StardewMap;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Egg
{
	private static final Logger logger = LogManager.getLogger(Egg.class);
	
	public static Egg sample(StardewMap map, int x, int y)
	{
		// find pixels
		// logger.debug("Sampling for egg on " + x + ", " + y);
		EggBuilder builder = new EggBuilder();
		builder.open(x, y);
		while (builder.hasNext())
		{
			Coordinate next = builder.next();
			int cx = next.getX();
			int cy = next.getY();
			builder.set(cx, cy, map.isEgg(cx, cy));
		}
		
		// find outline
		Set<Coordinate> pixels = builder.getLocations();
		int x1 = Integer.MAX_VALUE;
		int y1 = Integer.MAX_VALUE;
		int x2 = Integer.MIN_VALUE;
		int y2 = Integer.MIN_VALUE;
		for (Coordinate coordinate : pixels)
		{
			int cx = coordinate.getX();
			int cy = coordinate.getY();
			x1 = Math.min(x1, cx);
			y1 = Math.min(y1, cy);
			x2 = Math.max(x2, cx);
			y2 = Math.max(y2, cy);
		}
		
		// make egg
		Egg egg = new Egg(x1, y1, x2 + 1, y2 + 1);
		for (Coordinate coordinate : pixels)
		{
			int cx = coordinate.getX() - egg.getX();
			int cy = coordinate.getY() - egg.getY();
			egg.pixels[cx][cy] = true;
		}
		Coordinate center = egg.getCenterCoordinate();
		logger.debug("Generated egg on " + center.getX() + ", " + center.getY() + " (" + pixels.size() + " pixels)");
		return egg;
	}
	
	private final int x1;
	private final int y1;
	private final int x2;
	private final int y2;
	private final boolean[][] pixels;
	
	public Egg(int x1, int y1, int x2, int y2)
	{
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
		this.pixels = new boolean[getWidth()][getHeight()];
	}
	
	public int getX()
	{
		return x1;
	}
	
	public int getY()
	{
		return y1;
	}
	
	public int getX2()
	{
		return x2;
	}
	
	public int getY2()
	{
		return y2;
	}
	
	public double getCenterX()
	{
		return (getX() + getX2()) / 2.0;
	}
	
	public double getCenterY()
	{
		return (getY() + getY2()) / 2.0;
	}
	
	public Coordinate getCenterCoordinate()
	{
		return new Coordinate((int) getCenterX(), (int) getCenterY());
	}
	
	public int getWidth()
	{
		return x2 - x1;
	}
	
	public int getHeight()
	{
		return y2 - y1;
	}
	
	public boolean isAt(int x, int y)
	{
		if (x < getX() || y < getY())
		{
			return false;
		}
		if (x > getX2() || y > getY2())
		{
			return false;
		}
		int px = x - getX();
		int py = y - getY();
		return pixels[px][py];
	}
	
	@Override
	public String toString()
	{
		return "Egg [x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + ", width=" + getWidth() + ", height=" + getHeight() + "]";
	}
}
