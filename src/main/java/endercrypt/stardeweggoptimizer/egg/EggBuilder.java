package endercrypt.stardeweggoptimizer.egg;


import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import endercrypt.library.commons.position.Coordinate;


public class EggBuilder implements Iterator<Coordinate>
{
	private final Set<Coordinate> open = new HashSet<>();
	private final Set<Coordinate> available = new HashSet<>();
	private final Set<Coordinate> processed = new HashSet<>();
	
	public void open(int x, int y)
	{
		Coordinate coordinate = new Coordinate(x, y);
		open.add(coordinate);
		available.remove(coordinate);
		processed.add(coordinate);
		
		for (Coordinate neighbour : getNeighbours(coordinate))
		{
			if (processed.contains(neighbour) == false)
			{
				available.add(neighbour);
			}
		}
	}
	
	public void close(int x, int y)
	{
		Coordinate coordinate = new Coordinate(x, y);
		open.remove(coordinate);
		available.remove(coordinate);
		processed.add(coordinate);
	}
	
	public void set(int x, int y, boolean value)
	{
		if (value)
		{
			open(x, y);
		}
		else
		{
			close(x, y);
		}
	}
	
	@Override
	public boolean hasNext()
	{
		return available.isEmpty() == false;
	}
	
	@Override
	public Coordinate next()
	{
		Iterator<Coordinate> iterator = available.iterator();
		Coordinate next = iterator.next();
		iterator.remove();
		return next;
	}
	
	public boolean isComplete()
	{
		return open.isEmpty() == false && available.isEmpty();
	}
	
	public Set<Coordinate> getLocations()
	{
		return Collections.unmodifiableSet(open);
	}
	
	private static Coordinate[] getNeighbours(Coordinate coordinate)
	{
		Coordinate[] result = new Coordinate[4];
		result[0] = coordinate.add(1, 0);
		result[1] = coordinate.add(-1, 0);
		result[2] = coordinate.add(0, 1);
		result[3] = coordinate.add(0, -1);
		return result;
	}
	
	@Override
	public String toString()
	{
		return EggBuilder.class.getSimpleName() + "[open=" + open.size() + ", processed=" + processed.size() + ", available=" + available.size() + "]";
	}
}
